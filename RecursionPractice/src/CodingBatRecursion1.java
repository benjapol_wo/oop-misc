public class CodingBatRecursion1 {
	static int level = 0;

	public static void main(String[] args) {
		System.out.println(strCount("xyxy", "x"));
	}

	public static long factorial(long n) {
		if (n <= 1)
			return n;
		return n * factorial(n - 1);
	}

	public static int bunnyEars(int bunnies) {
		if (bunnies <= 0)
			return 0;
		return 2 + bunnyEars(bunnies - 1);
	}

	public static int fibonacci(int n) {
		if (n == 0)
			return 0;
		if (n == 1)
			return 1;
		return fibonacci(n - 2) + fibonacci(n - 1);
	}

	public static int bunnyEars2(int bunnies) {
		if (bunnies <= 0)
			return 0;
		if (bunnies % 2 == 1)
			return 2 + bunnyEars2(bunnies - 1);
		return 3 + bunnyEars2(bunnies - 1);
	}

	public static int triangle(int rows) {
		if (rows <= 0)
			return 0;
		return rows + triangle(rows - 1);
	}

	public static int sumDigits(int n) {
		if (n == 0)
			return n;
		return n % 10 + (sumDigits(n / 10));
	}

	public static int count7(int n) {
		if (n == 0)
			return n;
		if (n % 10 == 7)
			return 1 + count7(n / 10);
		return count7(n / 10);
	}

	public static int count8(int n) {
		if (n == 0)
			return n;
		if (n % 10 == 8) {
			if ((n / 10) % 10 == 8)
				return 2 + count8(n / 10);
			return 1 + count8(n / 10);
		}
		return count8(n / 10);
	}

	public static int powerN(int base, int n) {
		if (n == 0)
			return 1;
		return base * powerN(base, n - 1);
	}

	public static int countX(String str) {
		if (str.length() == 0)
			return 0;
		if (str.substring(0, 1).equals("x"))
			return 1 + countX(str.substring(1));
		return countX(str.substring(1));
	}

	public static int countHi(String str) {
		if (str.length() <= 1)
			return 0;
		if (str.substring(str.length() - 2).equals("hi"))
			return 1 + countHi(str.substring(0, str.length() - 2));
		return countHi(str.substring(0, str.length() - 1));
	}

	public static String changeXY(String str) {
		if (str.isEmpty())
			return str;
		if (str.startsWith("x"))
			return "y" + changeXY(str.substring(1));
		return str.substring(0, 1) + changeXY(str.substring(1));
	}

	public static String changePi(String str) {
		if (str.length() <= 1)
			return str;
		if (str.startsWith("pi"))
			return "3.14" + changePi(str.substring(2));
		return str.charAt(0) + changePi(str.substring(1));
	}

	public static String noX(String str) {
		if (str.isEmpty())
			return str;
		if (str.startsWith("x"))
			return noX(str.substring(1));
		return str.charAt(0) + noX(str.substring(1));
	}

	public static boolean array6(int[] nums, int index) {
		if (index >= nums.length)
			return false;
		if (nums[index] == 6)
			return true;
		return false || array6(nums, index + 1);
	}

	public static int array11(int[] nums, int index) {
		if (index >= nums.length)
			return 0;
		if (nums[index] == 11)
			return 1 + array11(nums, index + 1);
		return array11(nums, index + 1);
	}

	public static boolean array220(int[] nums, int index) {
		if (index >= nums.length - 1)
			return false;
		if (nums[index] * 10 == nums[index + 1])
			return true;
		return false || array220(nums, index + 1);
	}

	public static String allStar(String str) {
		if (str.length() <= 1)
			return str;
		return str.charAt(0) + "*" + allStar(str.substring(1));
	}

	public static String pairStar(String str) {
		if (str.length() <= 1)
			return str;
		if (str.charAt(0) == str.charAt(1))
			return str.charAt(0) + "*" + pairStar(str.substring(1));
		return str.charAt(0) + pairStar(str.substring(1));
	}

	public static String endX(String str) {
		if (str.length() <= 1)
			return str;
		if (str.startsWith("x"))
			return endX(str.substring(1)) + "x";
		return str.charAt(0) + endX(str.substring(1));
	}

	public static int countPairs(String str) {
		if (str.length() <= 2)
			return 0;
		if (str.startsWith(Character.toString(str.charAt(2))))
			return 1 + countPairs(str.substring(1));
		return countPairs(str.substring(1));
	}

	public static int countAbc(String str) {
		if (str.length() <= 2)
			return 0;
		if (str.startsWith("aba") || str.startsWith("abc"))
			return 1 + countAbc(str.substring(1));
		return countAbc(str.substring(1));
	}

	public static int count11(String str) {
		if (str.length() <= 1)
			return 0;
		if (str.startsWith("11"))
			return 1 + count11(str.substring(2));
		return count11(str.substring(1));
	}

	public static String stringClean(String str) {
		if (str.length() <= 1)
			return str;
		if (str.charAt(0) == str.charAt(1))
			return stringClean(str.substring(1));
		return str.charAt(0) + stringClean(str.substring(1));
	}

	public static int countHi2(String str) {
		if (str.length() <= 1)
			return 0;

		// Cutting from the index 0 method
		// if (str.startsWith("x")) {
		// if (str.charAt(1) == 'x') return countHi2(str.substring(1));
		// return countHi2(str.substring(2));
		// }
		// if (str.substring(0, 2).equals("hi")) return 1 +
		// countHi2(str.substring(2));
		// return countHi2(str.substring(1));

		// Cutting from the last index method
		if (str.substring(str.length() - 2).equals("hi")) {
			if (str.length() > 2 && str.charAt(str.length() - 3) == 'x')
				return countHi2(str.substring(0, str.length() - 3));
			return 1 + countHi2(str.substring(0, str.length() - 2));
		}
		return countHi2(str.substring(0, str.length() - 1));
	}

	public static String parenBit(String str) {
		if (str.length() <= 0)
			return str;
		if (str.startsWith("("))
			if (str.charAt(1) == ')')
				return "()";
			else
				return "(" + str.charAt(1) + parenBit("|" + str.substring(2));
		if (str.startsWith("|"))
			if (str.charAt(1) == ')')
				return ")";
			else
				return str.charAt(1) + parenBit("|" + str.substring(2));
		return parenBit(str.substring(1));

		// Solution
		// if (str.charAt(0) != '(') {
		// return parenBit(str.substring(1));
		// }
		// if (str.charAt(str.length()-1) != ')') {
		// return parenBit(str.substring(0, str.length()-1));
		// }
		// return str;
		// Solution notes: this is tricky. Is the first char a '('?
		// If not, recur, removing one char from the left of the string.
		// Eventually this gets us to '(' at the start of the string.
		// If the first char is '(', then recur similarly, removing one char
		// from the end of the string, until it is ')'.
		// Now the first and last chars are ( .. ) and you're done.

	}

	public static boolean nestParen(String str) {
		if (str.length() <= 0)
			return true;
		if (str.charAt(0) == '(') {
			if (str.charAt(str.length() - 1) == ')')
				return true && nestParen(str.substring(1, str.length() - 1));
		}
		return false;
	}

	public static int strCount(String str, String sub) {
		if (str.length() < sub.length())
			return 0;
		if (str.startsWith(sub))
			return 1 + strCount(str.substring(sub.length()), sub);
		return strCount(str.substring(1), sub);
	}

	public static boolean strCopies(String str, String sub, int n) {
		if (str.length() <= 0)
			if (n > 0)
				return false;
			else
				return true;
		if (str.startsWith(sub))
			return strCopies(str.substring(1), sub, n - 1);
		return strCopies(str.substring(1), sub, n);
	}

	public static int strDist(String str, String sub) {
		if (str.length() < sub.length())
			return 0;
		if (str.startsWith(sub))
			if (str.endsWith(sub))
				return str.length();
			else
				return strDist(str.substring(0, str.length() - 1), sub);
		return strDist(str.substring(1), sub);
	}
}